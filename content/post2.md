Title: Lần đầu gặp Ngọc Trinh
Date: 2020-09-27
Category: Trang chủ
Tags: pymi, among us,
Slug: second
Authors: A Pymier
Summary: page2

## Lần đầu gặp Ngọc Trinh
Hôm nay, trong khi mình đang đi loanh quoanh trong tàu vũ trụ để hoàn thành từng hạn mục nhiệm vụ được giao thì vô tình bắt gặp Ngọc Trinh.
Toàn thân em ấy hồng hào, đầu cài bông hoa lá cành...

Trong tâm trạng ngất ngây vì gặp được Ngọc Trinh bằng xương bằng thịt, tôi cứng đờ người, cơ thể không sao cử động được. Mãi một lúc sau đó tôi mới kịp định thần trở lại. Khi đó, tôi bắt đầu mở lời rủ em cùng đi làm nhiệm vụ. Cũng trong giây phút ấy, không nao núng, Ngọc Trinh đạp tôi sấp mặt xuống đất và rút hung khí ra để thủ tiêu tôi, một phát rất ngọt.

Thì ra, em là người được chọn, em là Impostor.

Và rồi tiếng nhạc cất lên...

## Cảm giác lúc ấy sẽ ra sao
Cảm giác lúc ấy sẽ ra sao?
Tự hỏi rằng sẽ khó thế nào?
Tạm biệt người yêu trong hồi ức
Tạm biệt tình yêu nơi tiềm thức
Đã bao nhiêu lần nếm cay đắng khi yêu
Cảm giác lúc ấy sẽ ra sao?
Cô đơn lên ngôi suốt bao đêm dài
Tạm biệt người yêu thương nhiều nhất
Tạm biệt người mang bao phiền phức
Đến con tim này
Hay là ta mới yêu chưa kịp đậm sâu

<img class="responsive" width="80%" alt="1 minute after the moment she killed me" src="https://gitlab.com/thevivotran/website/-/raw/master/content/images/photo_2020-09-27_21-22-21.jpg">